require 'guard'
require 'guard/commander'

task :build do
  mkdir_p 'build'
  FileUtils.cp 'otros/bibliografia.bib', 'build/memoria.bib'
  Dir.chdir 'build'
  capitulos = Dir.glob('../memoria/*.md')
                 .sort
                 .map {|file| '"' + file + '"'}
                 .join(' ')

  sh "pandoc --template ../otros/template.tex --top-level-division=chapter --listings #{capitulos} -t latex -o memoria.tex"

  puts 'Llamando a latex 1/6'
  sh 'latex memoria.tex'
  puts 'Incluyendo la bibliografia 2/6'
  sh 'biber memoria'
  puts 'Llamando a glosaries 3/6'
  sh 'makeglossaries memoria'
  puts 'Llamando a latex 4/6'
  sh 'latex memoria.tex'
  puts 'Incluyendo graficos 5/6'
  sh 'dvips -o memoria.ps memoria.dvi'
  puts 'Pasando a PDF 6/6'
  sh 'ps2pdf memoria.ps memoria.pdf'
  Dir.chdir '..'
end

task :clean do
  exts = [
    'toc',
    'ps',
    'log',
    'lof',
    'lot',
    'dvi',
    'aux',
    'blg',
    'bbl',
    'out',
    'tex',
    'pdf'
  ]

  Dir.glob("*.{#{exts.join(',')}}").each {|file| File.delete(file)}
end

task default: [:build]

namespace :build do
  desc 'Observa los ficheros y construye el pdf'
  task :watch do
    Guard.start
  end
end

