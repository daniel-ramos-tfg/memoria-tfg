# Conclusiones y líneas futuras

## Conclusiones

La configuración de una dieta alimenticia es un problema complejo en el que hay que tener en cuenta muchos aspectos. Además existen multitud de opciones y recetas válidas para cumplir un objetivo alimentario, pero elegir cual es la más adecuada es un problema más profundo.

Con el desarrollo de este Lenguaje de Dominio Específico se intenta facilitar y unificar la forma de describir un menú dietético, y mediante el desarrollo del módulo de generado de recetas, construir un ecosistema para que instituciones como colegios e institutos puedan ofrecer a sus alumnos dietas equilibradas.

Al estar desarrollado en Ruby, es sencillo de integrar con una aplicación de Ruby on Rails ya que usan el mismo lenguaje. Se podría llegar a integrar con servidores PHP usando una llamada `system`, o incluso con Node.js mediante un transpilador como *"Rubular"*.

## Líneas futuras

Aunque se ha completado el desarrollo del DSL, existen distintos aspectos de ambas gemas que quedan por mejorar. A continuación enumeraremos unas líneas futuras con las que se puede continuar el desarrollo del proyecto:

### DSL

* Añadir nuevos métodos a las clases que ayuden a especificar los menús.
* Internacionalizar correctamente la gema: En algunas partes se usa inglés y otras en español. Se podría crear una versión de la gema "principal" escrita completamente en inglés, y luego una capa superior en español.
* Separar en librerías distintas el \acrshort{orm}, las medidas y el DSL de alimentos. Creo que quedaría mejor si estuviese más modularizado.
* Intentar integrar las medidas con *Alchemist*. Se ha recogido una interfaz similar a esta librería para representar las medidas, quizás se puede realizar una bifurcación y ampliar la funcionalidad con la que se ha implementado en este proyecto.
* Buscar una base de datos alternativa. El problema de usar \acrshort{bedca} es que hay algunos alimentos que no están disponibles en su página web, además de que no existe ninguna documentación sobre su API. Además, hace que sea necesario tener una conexión a Internet para poder usar la gema. Se propone replicar los datos y proporcionarlos como servicio web.
* Documentar el uso de la gema.

### Herramienta de visualización

* Ofrecer parámetros para especificar el directorio de la plantilla así como de la hoja de estilos.
* Añadir capacidad para exportar en otros formatos, como JSON, XML o YAML.
* Crear una interfaz gráfica interactiva para poder crear los menús mediante bloques.
* Añadir más pruebas.
* Documentar el uso de la herramienta.

### Generado de menús

Este módulo queda fuera del alcance del TFG, pero es una de las partes clave para que el sistema pueda funcionar.

### Distribución como microservicio

Se podría distribuir el ecosistema final en forma de microservicio como una imagen de Docker. De esta forma, cuando se vaya a hacer uso de la aplicación sólo haría falta desplegarlo como un contenedor para poder acceder a la aplicación, y no haría falta instalar todas las dependencias del programa.
