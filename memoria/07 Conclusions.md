# Conclusions

The configuration of a diet is a complex problem in which we must watch out many aspects. In addition there are many options and recipes valid to meet a food objective, but choosing which is the most appropriate is a deeper problem.

With the development of this Domain Specific Language we try to facilitate and unify the way to describe a dietary menu, and by developing the module of recipe generation, build an ecosystem so that institutions such as schools and high schools can offer their students balanced diets.

As is written in Ruby, it's simple to integrate with a Ruby on Rails application since they use the same language. Also, it could be integrated with PHP servers using a `system` call, or even with Node.js using a transpiler like *"Opal"*.
