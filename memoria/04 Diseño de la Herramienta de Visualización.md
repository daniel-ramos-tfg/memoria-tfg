# Diseño de la Herramienta de Visualización

Una vez acabado el desarrollo de la gema `dietary_dsl`, se propuso hacer una herramienta para poder visualizar la salida del Lenguaje de Dominio Específico. Se ha separado del repositorio principal distribuyéndolo también como una gema separada, `dietary_dsl_viewer`, para poder diferenciar correctamente el DSL de la capa de presentación. El programa se usa desde la línea de comandos mediante el binario `rubofood`, y toma como entrada un fichero con el DSL, y como salida genera un `index.html` y un `style.css`.

## ERB

\acrfull{erb}, es un motor de renderizado mediante el uso de plantillas para Ruby, que está incluido en el propio lenguaje sin necesidad de librerías externas. Con él se puede embeber código ruby dentro de un fichero arbitrario, aunque comúnmente se usa para HTML.

Para poder representar los datos, internamente la gema sigue varios pasos:

1. Lee el fichero que se le pasa por los argumentos, debe ser un fichero con código Ruby.
2. Evalúa el contenido del fichero en el ámbito actual mediante `instance_eval`.
3. Se pasa el contexto al motor de ERB para obtener un String con código HTML.
4. Se vuelca el contenido en un `index.html`, y se copia fichero `style.css`.

Para que el contexto pasado a ERB sea correcto, debemos guardar el `Dia`, o un Array de los mismos en la variable `@exports`, para que pueda ser leída por el motor de renderizado. Se puede encontrar un ejemplo real y complejo en el directorio `spec/dietary_dsl_viewer/menu_example/menu.rb` de esta gema.

\begin{figure}
\centering
\includegraphics[width=10cm,height=18cm,keepaspectratio]{../objects/salida-dietary-dsl-viewer.eps}
\caption{Ejemplo de visualización de un menú de un lunes}
\end{figure}

En caso de añadir más días, se irán apilando de forma horizontal, por lo que podremos representar una dieta semanal.

## CLI con Thor

Para poder hacer la interfaz de comandos, se ha usado Thor. Thor es una gema de Ruby que permite crear interfaces de comandos complejas de forma muy sencilla. Simplemente tenemos que crear una clase heredando la que proporciona la librería y llamar a algunos métodos. Estos métodos funcionan de forma similar a los decoradores de otros lenguajes, ampliando la funcionalidad de los métodos.

Una vez tenemos el fichero de configuración de la CLI completado, tendremos una interfaz completa, con ayuda, parámetros e incluso documentación para `man`.

![Ayuda de la interfaz de comandos de la gema](../objects/cli.eps)

Se han incluido los siguientes comandos para la primera versión:

* `rubofood --version, -v`: Imprime la versión actual de la gema.
* `rubofood help [Comando]`: Imprime la ayuda si no se le pasa ningún comando, e imprime ayuda específica para un comando si se le pasa.
* `rubofood visualize FICHERO`: Genera la visualización a partir de una especificación de una dieta.

## Pruebas

Las pruebas funcionales de la gema se han hecho con un desarrollo dirigido por pruebas con el uso de Rspec, casando con expresiones regulares las expectativas de la salida que obtenemos en el fichero HTML.

Por otro lado, para probar el correcto funcionamiento de la interfaz de comandos se ha usado Cucumber junto con las definiciones de pasos de la librería Aruba. Hablaremos de estas herramientas en el próximo apartado.
