# Introducción

## ¿Qué es una dieta equilibrada?

Según la Encuesta Europea de Salud en España de 2014 publicada por el Instituto Nacional de Estadística \cite{EncuestaEuropeaDeSalud}, existe una tendencia en el aumento de sobrepeso, y este problema no sólo se da en España, el ritmo de crecimiento de obesidad es alarmante, el cual se ha duplicado desde 1980 \cite{ObesidadYSobrepesoOMS}.

La obesidad supone un riesgo para la salud tanto física como mental, pudiendo provocar enfermedades como hipertensión, diabetes, problemas del hígado e incluso cáncer en los casos más extremos. Actualmente, éstas y algunas otras enfermedades crónicas son la principal causa de mortalidad del mundo, esto son el 63% de las muertes, siendo responsables de 36 millones de muertes cada año \cite{EnfermedadesCronicasOMS}. Aun peor, en algunos países como Canadá la situación es más preocupante, donde el porcentaje de mortalidad alcanza el 90% \cite{TheFutureOfHealth}.

Estos datos sobre la obesidad y sobrepeso son muy alarmantes y deberían inducirnos a llevar un estilo de vida saludable, pero también debemos nombrar algunos de los beneficios que nos puede proporcionar una dieta equilibrada:

* Sistema inmunológico: Garantizar los nutrientes necesarios para el sistema inmunológico es muy importante, y que éste es dependiente del flujo sanguíneo por lo que necesitamos una buena función vascular. Carecer de ciertos nutrientes como pueden ser la Vitamina C, E y K tienen una repercusión negativa sobre el sistema inmunológico, ya que éstos nos ayudan a tener un buen flujo sanguíneo. Además otros nutrientes como la Vitamina C, E, hierro y ácidos grasos omega 3 nos permiten tener una buena producción de glóbulos blancos.

* Ayuda al sistema digestivo, ya que una dieta con una buena cantidad de fibra y baja en grasas facilita la digestión ya que no es tan alta la cantidad de enzimas para poder digerirlos.

* Reduce el padecimiento de trastornos mentales, como la ansiedad o la depresión. Determinados nutrientes como el fósforo nos ayudan a tener un cerebro rico y sano. Pero también desde un punto de vista social mejora nuestro autoestima y autoconcepto \cite{SobrepesoYObesidadSocial}.

Pero, **¿Qué exactamente una dieta equilibrada?** Según la Sociedad Española de Dietética y Ciencias de la Alimentación \cite{LaDietaEquilibrada}:

> *Una dieta equilibrada es aquella manera de alimentarse que aporta alimentos variados en cantidades adaptadas a nuestros requerimientos y condiciones personales*.

Es decir, en una dieta equilibrada se consumen los alimentos que demanda nuestro cuerpo y le suplen los minerales, vitaminas y nutrientes que necesita para poder tener un buen estado de salud.

## Herramientas para la visualización de menús

Se ha llevado a cabo una revisión bibliográfica de herramientas que permitan visualizar menús dietéticos.

Se ha encontrado diversa información haciendo referencia al modelado de menús dietéticos, pero no desde una aproximación de un Lenguaje de Dominio Específico (en inglés \acrshort{dsl}, \acrlong{dsl}), sino basados en ontologías y en motores de reglas complejas, como puede ser el trabajo de *"FOODS: A Food-Oriented Ontology-Driven System"* \cite{FOODSOntologia}.

El artículo de investigación más similar al objetivo de este proyecto que se ha encontrado es *"Genetic Fuzzy Markup Language for Diet Application"* \cite{FuzzyMarkupForDiet}, donde se habla de los lenguaje difusos para la elaboración de una aplicación dietética. Tiene sentido hablar de este tipo de lenguajes, ya que para especificar un menú dietético no existen límites determinados, y es por esto que es necesario aproximarse al problema de generación de dietas desde un punto de vista inexacto. Un ejemplo de esta aproximación es el lenguaje *"Fuzzy Control Language"*, el cual es un lenguaje que implementa lógica difusa donde se pueden realizar expresiones inexactas como `IF (Car IS Inside) THEN (Door IS Down)`. El objetivo de este lenguaje son sistemas industriales donde los sensores tienen cierto margen de error.

Respecto a los \acrshort{dsl}, se analizó el trabajo *"A Programming Environment for Visual Block-Based Domain-Specific Languages"* \cite{VisualDSL}, donde se habla del uso de un lenguaje de dominio específico mediante el uso de la programación visual basada en bloques.

Por último, cabe destacar el trabajo *"Ontology-based multi-agents for intelligent healthcare applications"* \cite{OntologyMultiAgents}. En el se usa una ontología basada en múltiples agentes para desarrollar una aplicación de salud inteligente. En este trabajo también se hace uso de interfaces borrosas para representar el conocimiento, mediante el uso del *Fuzzy markup language*.

Existe más documentación sobre estos temas, pero no se encontró ninguna referencia al uso de lenguajes de dominio específico para la elaboración de menús dietéticos.

## Objetivos del proyecto

El objetivo general del \acrfull{tfg} es diseñar un Lenguaje de Dominio Específico - \acrshort{dsl} - para modelar menús dietéticos. Se han establecido tres objetivos específicos:

* **Lenguaje de Dominio Específico**: Por un lado, tenemos el lenguaje que hemos diseñado y desarrollado, que es un DSL interno del lenguaje de programación Ruby. Con él se pueden especificar de una forma similar al lenguaje humano recetas y menús dietéticos. El DSL nos sirve como interfaz para los dos siguientes módulos.

* **Visualización de los menús**: La tarea de este módulo es presentar de forma gráfica el significado del DSL, de forma que teniendo un menú descrito mediante el lenguaje de dominio específico, se puede obtener una tabla con las distintas comidas y sus valores nutricionales. Se han usado como modelo los menús de PIPO, el Programa de Intervención para la Obesidad Infantil \cite{MenusPipoPorEdad}.

* **Generado de menús**: Ambos módulos se integran con un generador automático de menús, que forma parte de un proyecto de mayor embergadura. Dicho sistema tendría la capacidad de generar menús equilibrados y saludables, por ejemplo mediante aprendizaje profundo o un motor de análisis complejo, de forma que genere recetas del DSL de Ruby.

## Metodología

Cuando se desarrolló el anteproyecto, se especificaron los distintos pasos a cumplir el desarrollo del \acrshort{tfg}. Han consistido en cumplir distintas tareas a lo largo del desarrollo del curso. Éstas son:

### Tarea 0. Coordinación

Esta tarea se estableció al comienzo del \acrshort{tfg}, y consistió en establecer las fechas de los seguimientos y reuniones que se iban a realizar durante su desarrollo. Éstas fechas no se llegaron a cumplir en su totalidad debido al consumo de tiempo de las prácticas externas, por lo que se tuvo que retrasar el cuarto seguimiento para finales de junio, y la defensa para el mes de julio.

### Tarea 1. Revisión bibliográfica

Se realizó una búsqueda en la base de datos de bibliotecas digitales para seleccionar documentos que guardaban relación con el objetivo del TFG. Para ello se consultaron las bases de datos de *Scopus*, *Web of Science*, *IEEE Xplore* y *Google Scholar*. La literatura seleccionada se ha guardado en el repositorio principal.

### Tarea 2. Diseño del prototipo de la herramienta

Se decidieron los detalles técnicos sobre la herramienta y su definición. Se barajó la posibilidad de crear un DSL externo en Python mediante el uso de la librería *"Pyparsing"*, pero al final se decidió usar Ruby para el DSL, ya que en la asignatura de Lenguajes y Paradigmas de Programación se aprendió a desarrollar en este lenguaje, así como a hacer un \acrshort{dsl} interno.

En esta tarea también decidimos usar la \acrfull{bedca} sobre la de \acrfull{usda}, ya que la primera estaba muy completa y además incluye los alimentos en español e inglés.

### Tarea 3. Implantación de la herramienta

En esta tarea se realizó un análisis de las herramientas de desarrollo que existen en el mercado.

* Se planteó usar Trello o Github Projects para organizar y dirigir el proyectos, usando finalmente los \glspl{issue} de Gitlab para anotar los problemas que surgían durante el desarrollo de la \Gls{gema}, así como características nuevas que se añadirían posteriormente.

* LaTeX como lenguaje para realizar el desarrollo tanto de la memoria como de la presentación. Se propuso usar la herramienta Pandoc para realizar el desarrollo de la memoria de forma sencilla, ya que con esta herramienta se puede escribir el documento en formato Markdown ya que ésta nos permite convertirlo a LaTeX.

* Como complemento a LaTeX, se usará BibTeX para almacenar las referencias bibliográficas.

* Se decidió usar Git como sistema para el control de versiones, y Github para alojar el repositorio. Aunque se ha seguido usando el repositorio de Github, ahora se usa únicamente como espejo del que está alojado en Gitlab, donde se mantiene el código fuente (issues, wikis...). Estas características se deshabilitaron en Github para mantener este tipo de información en Gitlab.

### Tarea 4. Validación y resultados computacionales

Se seguirá un desarrollo dirigido por pruebas usando distintas herramientas. Inicialmente se desconocía el lenguaje en el que iba a ser desarrollado el lenguaje, pero como finalmente ha sido Ruby se han utilizado las herramientas que nos proporciona la comunidad de este lenguaje.

* Automatización de pruebas: Se ha usado *"Rspec"* para hacer un desarrollo dirigido por pruebas, siguiendo el flujo de trabajo que corresponde.

* Integración Continua: Finalmente se han usado los *"\glspl{runner}"* de Gitlab para montar un sistema de integración continua, donde es posible obtener una revisión para cada confirmación (\gls{commit}) que hagamos, así como en las posibles peticiones de extracción (\gls{pullRequest}) que puedan darse. Además, es una validación de que el código funciona correctamente.

* Cubrimiento de código: En un principio se planteó usar *"Coveralls"* para realizar el análisis de cubrimiento de las pruebas, pero Gitlab ofrece este servicio en forma de *"\gls{runner}"* así como alojar el informe en la página web del repositorio.

* Análisis estático de código: Usar una herramienta de análisis de código (también conocidos como *"\glspl{linter}"*), puede mejorar nuestro estilo de escribir un determinado programa. Especialmente en este caso, ha servido para indicar las distintas deficiencias en el diseño que podrían ralentizar el desarrollo o aumentar el riesgo de errores o fallos en el futuro (*\glspl{smell})* que tenía en el código así como características nuevas que se han incorporando al lenguaje que no se estaban usando. Para Ruby existe el demonimado *"Rubocop"*, una herramienta con múliples opciones capaz de analizar errores de anidamiento, complejidad ciclomática, características del lenguaje que están obsoletas, etc.

### Tarea 5. Difusión de los resultados

Se elaborará la presentación del proyecto para presentarlo ante el tribunal y en el Congreso de Estudiantes de Informática. Así mismo, se tiene pensado participar con él en la \acrfull{tlp}. Además el código fuente se liberará bajo una licencia MIT y la memoria y presentación en Creative Commons Reconocimiento 4.0 Internacional haciendo público el código tanto en Github como en Gitlab una vez esté finalizado el trabajo.
