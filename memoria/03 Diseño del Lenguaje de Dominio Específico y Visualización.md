# Diseño del Lenguaje de Dominio Específico

La \gls{gema} que compone el \acrshort{dsl} (`dietary_dsl`) está formada a su vez por otros \acrshort{dsl} más pequeños que cumplen distintas funciones para construir un buen ecosistema que permita representar lo mejor posible menús dietéticos. Estos son: la interfaz con la base de datos (\acrshort{orm}), la representación de medidas, y el \acrshort{dsl} de los alimentos.

## Mapeo Objeto-Relacional

Cuando se planteó desarrollar un \acrshort{dsl} para modelar menús dietéticos, uno de los primeros problemas que surgió fue el obtener los datos de los alimentos. Se decidió usar la base de datos \acrshort{bedca} frente \acrshort{usda} ya que la primera contiene los nombres en español de los alimentos, además de inglés.

Se quería desarrollar una interfaz similar a la de ActiveRecord, el famoso \acrshort{orm} de Ruby on Rails de forma que a un desarrollador de Ruby le sea lo más familiar posible esta interfaz. Se han conseguido imitar los siguientes métodos:

* `.find`: Devuelve un elemento buscando por su ID.
* `.find_by`: Devuelve el primer elemento cuyos elementos sean **exactamente iguales** a los proporcionados por parámetro.
* `.find_by_like`: Devuelve el primer elemento cuyos elementos cumplan la expresión regular proporcionada por parámetro.

![Comparación de una query en ActiveRecord y `dietary_dsl`](../objects/activerecord-dietary_dsl.eps)

El mayor inconveniente de haber usado BEDCA, es que no proporciona ninguna API ni documentación en su página web. Se ha tenido que hacer un trabajo de ingeniería inversa capturando las llamadas al servidor para ver si era posible crear un \gls{wrapper} ya sea mediante una API o un \gls{scrapper}.

Para capturar las peticiones HTTP que realiza la página web de BEDCA se ha usado la herramienta *Postman*. Con ella se pueden analizar todos los aspectos de una petición HTTP, como el cuerpo o la cabecera por ejemplo. Cuando se capturaron estas peticiones, resulta que el sitio web realizaba consultas a una API de XML escrita en PHP/5.2.4 con Ubuntu 5.10, por lo que esta aplicación debe datar  alrededor de 2007. Se intentó averiguar si se hacía uso de un framework para desarrollar la API por si ya existía un Wrapper para Ruby, pero se llegó a la conclusión de que se había escrito a mano.

![Llamada a la API de BEDCA](../objects/postman.eps)

No existe ninguna documentación sobre esta API, por lo que se ha tenido que ir averiguando que significan los distintos campos que estaban disponibles además de los distintos tipos de llamadas.

Se realizó una búsqueda por si ya existía alguna interfaz con esta base de datos, pero no se encontró ninguna por lo que se tuvo que desarrollar este \acrshort{orm} para poder realizar peticiones a la base de datos.


### `Food`

Esta clase es la interfaz principal del \acrshort{orm}. Además de los métodos para hacer consultas, tiene sobrecargado el operador de indexación (`[]`) para poder acceder a los valores del alimento, por ejemplo `manzana[:nombre]` devuelve "Manzana", y `manzana[:name]` "Apple", y así sucesivamente.

El comportamiento interno del \acrshort{orm} a la hora de ejecutar una consulta es el siguiente:

1. Se llama al método con el que se quiera realizar la consulta.
2. Se analizan los parámetros y se llama al método interno que corresponda.
3. Se construye el documento XML para realizar la petición usando la gema `builder`.
4. Se realiza una petición HTTP con el documento XML y con las cabeceras necesarias.
5. Se lee la respuesta de la petición, se comprueba que es válida y se interpreta el XML con la gema `activesupport`.
6. Se instancia la clase `Food` con los datos de la petición.

Cuando el constructor de la clase `Food` recibe los datos, convierte las claves en forma de *string* del Hash en símbolos para que sean únicos, y se agrupan los valores nutricionales por categoría creando instancias de la clase `FoodValues`.

Otro método útil es `kcal`, con el que se pueden calcular las kilocalorías totales del alimento (hay que calcularlo porque la base de datos proporciona este dato en kilojulios).

Además, se incluye el método `to_s` que exporta el alimento en formato Markdown. En el repositorio de `dietary_dsl`, bajo el directorio `spec/dietary_dsl/bedca_api/fixtures/pulpo_output.md` se puede encontrar un ejemplo de la salida.

### `FoodValues`

Esta clase representa una categoría de valores nutricionales, y almacena los datos de los mismos. Incluye el módulo `Enumerable` para poder recorrer cada uno de los valores nutricionales, y además incluye un método llamado `to_table` que permite obtener una tabla en formato Markdown con sus valores. Al igual que la clase `Food`, se sobrecarga el operador de indexación para poder acceder a cada valor nutricional.

El constructor recibe un array de valores nutricionales, el cual se recorre para crear instancias de la clase `FoodValue` (en singular).

### `FoodValue`

Aquí se almacenan los datos referentes a un valor nutricional específico, como su nombre, la unidad de medida, cantidad por cada 100 gramos, una descripción breve, y más atributos. Al igual que en las clases superiores se sobrecarga el operador para indexar, y se incluye el método `to_table`.

### Resultado

Con estas tres clases obtenemos una buena interfaz con BEDCA para que pueda ser consumida por el Lenguaje de Dominio Específico.

![Ejemplo de obtención de datos mediante el ORM](../objects/ejemplo-food.eps)

## Representación de Medidas

Otro de los problemas que nos hemos enfrentado, es a la especificación de medidas para los platos. Se realizó una búsqueda de una librería que permitiese especificar medidas como gramos y litros, y que además permitiese la conversión entre ellos. Se encontró únicamente la librería llamada Alchemist, la cual añadía nuevos métodos a la clase numérica primitiva de Ruby de tal forma que se pudiesen escribir de forma muy natural, como `2.miles.to.meters`. No se usó esta librería directamente porque se quería ampliar su funcionalidad para que la representación de algunas medidas de cocina (como cucharadas, o vasos), y además incluye conversiones de distancia, velocidades, y más que no es útil para el propósito de esta gema.

### `Measure`

Esta es la clase de la que heredan las demás. Incluye un constructor que inicializa la unidad de medida principal de la clase, un método `.to` que devuelve la propia clase (es azúcar sintáctica), y un método `to_s` para poder imprimir las medidas.

### `Masa`

Esta clase representa unidades de medidas de masa, como gramos o kilogramos, pero además se pueden representar usando medidas inexactas como tazas o rodajas. Se sobrecargan los operadores operacionales para poder representar medidas en forma de fracción o multiplicación.

Se abre la clase primitiva Fixnum para que estos métodos queden disponibles al instanciar un número.

### `Volumen`

Representa unidades de medidas relacionadas con el volumen, como litros, mililitros o centímetros cúbicos, y al igual que la clase `Masa` soporta medidas inexactas como cucharadas, vasos o chorritos.

### Resultado

Al abrir y expandir la clase Fixnum con estos nuevos métodos, obtenemos una interfaz similar a la que tiene Alchemist, con la que se pueden representar distintas unidades de medidas y realizar operaciones con ellas:

![Ejemplo de representación de medidas](../objects/measures.eps)

En la última línea se puede ver como lo que se ha devuelto es una instancia de Masa. Esto permite tener varios métodos a nuestro alcance, por ejemplo `to_s` para imprimirlo con formato.

## Representación de Menús Dietéticos

Esta es la parte más importante del Trabajo Final de Grado. Son las clases que ofrecen una interfaz idiomática para relacionarse con la base de datos y poder traer datos desde la misma, así como realizar operaciones sobre los mismos. Consta en total de cuatro clases que están contenidas unas dentro de otras, es decir, un Plato está conformado por distintos Alimentos, un Menu por varios Platos y un Día por varios Menús. Todas las clases incluyen el módulo `Enumerable`, por lo que se pueden recorrer y obtener todos sus datos.

### `Alimento`

![Ejemplo de dos instancias de Alimentos](../objects/alimento.eps)

Esta clase permite instanciar un Alimento en concreto, usando el método `.find_by_like` por detrás. Como vemos en el ejemplo, recibe por parámetro el nombre del Alimento, y el segundo parámetro que recibe el constructor son opciones para el \acrshort{orm}. Es casi un alias para la clase `Food`, solo que tiene algunos valores por defecto.

### `Plato`

![Ejemplo de instancia de Plato](../objects/plato.eps)

La clase Plato se compone de instancias de Alimentos. Permite almacenar pares de Alimentos y cantidades, y en base a estos datos se pueden realizar diferentes cálculos a través de sus métodos:

* `#kcal`: Calcula las kilocalorías totales del plato.
* `#kcal_por_cada(cantidad)`: Calcula las kilocalorías para una ración determinada de dicho plato.
* `#masa`: Devuelve la masa total del plato.
* `#single?`: Devuelve verdadero si el plato se compone de un único alimento, falso en otro caso.

### `Menu`

![Ejemplo de instancia de Menú](../objects/menu.eps)

Como aparece en la figura 3.7, al contrario que otras clases, un Menú se puede componer tanto de Platos como de Alimentos directamente. Esto permite evitar crear platos cuando tenemos alimentos individuales. Además, cuando usemos un plato, podremos especificar la ración para calcular correctamente las kilocalorías más tarde.

### `Dia`

![Ejemplo de instancia de un Día](../objects/dia.eps)

Por último, tenemos la clase que contiene a todas las demás. Permite agrupar los distintos Menús que hemos creado para asignarlos a distintos momentos del día. Se puede iterar sobre este objeto para obtener cada una de las cinco comidas, pero recibiremos un `nil` si no se ha establecido esa comida. También se pueden obtener las kilocalorías totales para este día.

### Resultado

Con el DSL para los menús conseguimos una interfaz sencilla, clara e idiomática con la que configurar un Menú dietético, que además se puede recorrer y consultar para lo que sea necesario, como un representación visual.
