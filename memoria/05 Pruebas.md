# Verificación y Pruebas

## Rspec

Para comprobar el correcto funcionamiento de ambas librerías, se ha usado Rspec para realizar las pruebas mediante un desarrollo dirigido por pruebas.

### Desarrollo dirigido por pruebas

El desarrollo dirigido por pruebas (en inglés \acrshort{tdd}, *Test Driven Development*) es una técnica de desarrollo especialmente usada en metodologías ágiles. Consta de cinco pasos:

1. Escribir las pruebas para el código.
2. Comprobar que las pruebas fallan.
3. Escribir el código necesario para superar las pruebas.
4. Volver a ejecutar las pruebas y se superan.
5. Refactorizar el código.

![Ejemplo de TDD mediante la gema `MiniTest`](../objects/tdd.eps)

### Desarrollo dirigido por comportamiento

El desarrollo dirigido por comportamiento (en inglés \acrshort{bdd}, *Behavior Driven Development*), es una forma de escribir los tests de \acrshort{tdd}, de forma que los tests sean idiomáticos y legibles. Por ejemplo, el test de la figura 5.1 se puede reecribir de la siguiente forma:

![Ejemplo de BDD](../objects/bdd-eng.eps)

Incluso, mediante el uso de los alias de Rspec se puede traducir y representarlo totalmente en español:

![Ejemplo de BDD con los alias de Rspec](../objects/bdd-esp.eps)

## Cucumber

Se ha usado este framework para realizar las pruebas de la interfaz de comandos de la herramienta de visualización. No existen demasiadas pruebas con Cucumber ya que la inclusión de las mismas fue al final del desarrollo de la herramienta.

No ha sido necesaria la definición de los pasos ya que se ha usado la librería *"Aruba"*. Esta gema proporciona muchos pasos útiles para Cucumber con el objectivo de probar las entradas, salidas, e interacciones con la interfaz de comandos.

## Menú generado para las pruebas

Finalmente, se ha realizado la definición de un Menú completo de un día para comprobar el correcto funcionamiento de ambas gemas. Se puede encontrar el fichero que describe el menú en el repositorio `dietary_dsl_viewer`, bajo el directorio `spec/dietary_dsl_viewer/menu_example/menu.rb`. La visualización se puede encontrar en la figura 4.1.
