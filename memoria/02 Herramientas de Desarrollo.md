# Herramientas de Desarrollo

## Ruby

Para implementar el diseño del \acrshort{dsl}, se ha usado Ruby ya que este lenguaje posee una gran interfaz para el programador, en comparación a otros lenguajes más antiguos como JavaScript. En algunos aspectos, JavaScript es muy similar a Ruby en lo que se refiere a herramientas de desarrollo, ya que tiene una gran comunidad detrás, en parte gracias al framework web Ruby on Rails. A continuación enumeraremos algunas de las herramientas de Ruby que se han usado para este proyecto y veremos su paralelismo con JavaScript.

* `rvm`: El *Ruby Version Manager* es una aplicación escrita para Bash (aunque tiene su equivalentes para otras shells) que permite cambiar entre las distintas versiones de Ruby de forma sencilla. Es muy útil si se tienen varios proyectos de Ruby en distintas versiones del intérprete. Además, mediante el fichero de configuración *rvmrc* se puede hacer un cambio automático de la versión de Ruby cuando en nuestro directorio de trabajo exista este fichero. El equivalente de JavaScript sería `nvm`.

* `rake`: Es una implementación en forma de DSL del programa `make` de GNU. Permite definir distintas tareas para automatizar la ejecución de determinados comandos. La definición de las tareas se realizan en un fichero `rakefile.rb`. Por ejemplo, para poder compilar esta memoria se han de realizar varios pasos. Éstos están definidos en el fichero de configuración, de forma que al ejecutar el comando `rake build` se construye el fichero PDF de la memoria, y de forma similar el comando `rake build:watch` vigila los ficheros Markdown y compila la memoria cada vez que haya cambios.

* `bundler`: *Bundler* es una \Gls{gema} que nos permite administrar distintos aspectos de las bibliotecas que vayamos a desarrollar:
    * Permite crear la estructura inicial de una gema de Ruby. Se va pasando por una serie de preguntas para poder configurar correctamente el proyecto. Tiene distintas formas de configuración mediante los parámetros que se le pasan a este comando como: la \gls{gema} que se va a usar para las pruebas, la licencia del proyecto o el código de conducta. Otro elemento útil de la estructura generada, es que crea una configuración inicial (*setup*) mediante Rake con muchas tareas útiles. También genera ficheros de configuración para la integración continua con Travis.
    * Administra las dependencias del proyecto mediante un fichero `.gemspec` que contiene toda la información de la biblioteca, como el nombre, descripción, etc... Una vez se ha clonado un proyecto de Ruby, con el comando `bundle install` esta gema se encargará de calcular las dependencias e instalarlas en el sistema.
    * Genera un fichero `Gemfile.lock`, cuyo propósito es bloquear la versión de las dependencias. De forma ideal, cuando se aumente el tercer dígito del versionado semántico (*\Gls{semver}*) sólo debería haber cambios para reparar errores. El problema es que cuando se trabaja en equipo, la versión de *bugfixing* puede variar de una máquina a otra, y esto puede generar errores a algunos miembros del equipo que están corregidos en otra versión. El fichero `Gemfile.lock` se asegura que las dependencias son exactamente las mismas en los distintos lugares donde tengamos nuestra aplicación Ruby. Se recomienda que este fichero se use cuando se están desarrollando aplicaciones, pero no para el desarrollo de \glspl{gema}, ya que obliga al usuario a hacer uso de una versión determinada, por lo que el árbol de dependencias crecería demasiado.

    ![Creación de una Gema con Bundler](../objects/bundler-gema.eps)

Estas herramientas son casi estándar en el desarrollo Ruby, a continuación veremos las gemas que han facilitado el desarrollo del TFG.

### Rubocop

*"Rubocop"* es una herramienta de análisis estático de código para Ruby, es decir un "*\gls{linter}*". Sin especificar ninguna configuración, es capaz de analizar con alrededor de 400 reglas (llamadas *"cops"*), pero además es una herramienta extensible donde se pueden modificar reglas ya existentes o incluso crear las propias nuestras. De entre las características que puede analizar *Rubocop*, cabe destacar:

* **Distribución**: Es capaz de analizar el indentado e indicar donde se está usando de forma inconsistente. También detecta elementos que no están alineados cuando se encadenan llamadas a funciones.
* **Código con alta probabilidad de error**: Condicionales ambiguos, claves de hash duplicadas, o interpolar cadenas en vez de concatenarlas.
* **Métricas**: ABCMetrics, complejidad ciclomática, anidamiento, longitud de líneas, etc.
* **Rendimiento**: Puede detectar código donde cambiando algunos elementos simples se puede conseguir un mejor rendimiento, esto es reutilizando variables, funciones redundantes, etc.
* **Estilo**: Son reglas que no provocan errores, pero al existir varias formas de escribir el mismo código estas reglas obligan a elegir una para que el código quede consistente en toda la aplicación.
* **Reglas específicas de gemas**: También contiene reglas que son específicas de determinadas librerías como Rails o Bundler.

![Ejemplo de ejecución de Rubocop donde detecta varios errores](../objects/rubocop.eps)

Un *\gls{linter}* es una herramienta indispensable para desarrollar una aplicación, especialmente cuando se trabaja en equipo o en proyectos de código abierto, ya que cada persona tiene su propia forma de escribir código y el \gls{linter} se encargará de asegurar que el estilo sea consistente a lo largo de todo el programa.

### Rspec

Rspec es un framework de pruebas para Ruby. Está orientado al \acrfull{bdd} con una interfaz para las pruebas que se asemeja a un diálogo.

![Ejemplo de prueba de la gema `dietary_dsl`](../objects/rspec-prueba.eps)

En la figura 2.3 se muestra una prueba del \acrfull{orm}, donde se espera que al buscar por un determinado campo en la base de datos nos devuelva el elemento correspondiente. Se pueden definir sujetos de prueba con los que testear nuestro código, darle un contexto a nuestras pruebas, etc. La interfaz de `expectations` tiene un gran número de métodos de forma que para casi todo lo que se quiera expresar se tiene un método o una combinación de los mismos que permiten expresarlo de forma idiomática. Incluso se puede renombrar los métodos de las pruebas para usarlo en otros idiomas y escribir "debe" en vez de "it" para describir algo como `debe 'devolver nil' do...`.

Rspec también incluye el denominado *"spec_helper"*, que es un fichero de configuración donde se indican todos los parámetros de las pruebas, así como la forma de enganchar librerías que amplíen su funcionalidad, como *\glspl{mock}* y *\glspl{stub}*.

### VCR y WebMock

Estas dos librerías sirven para capturar los accesos a internet de la gema durante las pruebas, (conocido como *\glspl{mock}* en inglés). Existen pruebas que verifican que se lanza el error correcto cuando existe un fallo. El problema que solucionan estas librerías es generar este fallo.

"*WebMock*" permite capturar todas las llamadas que vaya a hacer Ruby por Internet, y otorga la posibilidad de hacer respuestas personalizas. Incluso son programables para reaccionar de forma distinta ante determinados parámetros.

Por otro lado tenemos VCR (el nombre viene de *Video Cassette Recorder*). Esta librería usa *WebMock* como base. Cada vez que se acceda a Internet se pueden redirigir a lo que se denomina un "cassette", que es un fichero en formato \acrshort{yaml} donde se tienen los datos con los que responder la petición. Se puede usar el mismo *"cassete"* para varios tests, e incluso parametrizarlo.

Pero lo más interesante de esta librería, especialmente para el \acrshort{orm} que se ha desarrollado, es la caché de las llamadas. Se ha configurado de tal forma que cada vez que capture una llamada a Internet, guarda su fichero \acrshort{yaml} correspondiente, y la siguiente vez que se ejequte la misma prueba se usará la respuesta que estaba guardada. De esta forma, la segunda vez que se pasen las pruebas se tiene latencia cero respecto a Internet. La base de datos BEDCA para una llamada normal tiene una latencia de unos 500 milisegundos, que con la cantidad de pruebas que se tienen tarda casi diez minutos en ejecutar todas las pruebas. Usando esta biblioteca se reduce a un minuto y medio. Tarda tanto debido a la librería que se encarga de interpretar (*parsear*) las respuestas XML para traducirlas a objetos Ruby.

A las respuestas que se han guardado se les ha configurado un tiempo de expiración de siete días, por si cambia algo en la base de datos BEDCA poder modificar el \acrshort{orm} para que vuelva a funcionar.

### Guard

Guard es una \Gls{gema} que permite vigilar determinados ficheros, y reaccionar de la manera que se haya configurado. En el caso de `dietary_dsl`, se ha configurado de tal forma que cuando se cambie un fichero de código fuente, se ejecute el fichero de pruebas correspondiente, y si no se encuentra se va subiendo a los directorios superiores hasta que se encuentre alguna prueba. Personalmente, creo que Guard y Rspec están demasiado entrelazados como para separarlos. Comparándolo con otros *\glspl{taskrunner}* como `brunch` o `gulp` se ve que estas herramientas están un poco obsoletas.

### Cucumber

Al igual que Rspec, Cucumber es un frameworks para pruebas. Es aun más idiomático que Rspec, hasta tal punto que las mismas pruebas pueden servir como documentación. En Cucumber se pueden diferenciar dos aspectos, las características (*features*) y las definiciones de los pasos (*steps definitions*).

![La feature del visualizador de `dietary_dsl`](../objects/cucumber-feature.eps)

En las features se describen varios pasos que le dan un contexto a las pruebas:

* `Feature`: Este es el nombre de la característica que se está probando.
* `Scenario`: Es el supuesto donde se van a ejecutar las pruebas.
* `Given`: Define cuales son las entradas necesarias que necesita el programa para poder ejecutarse.
* `When`: El paso de ejecución, es lo que se debe realizar para obtener una salida.
* `Then`: Es el resultado que se espera obtener.

Pero, ¿Cómo se pueden ejecutar estas pruebas? Casando el texto con expresiones regulares mediante las definiciones de los pasos.

![Pasos para ejecutar el `Given` de la parte superior](../objects/cucumber-step-definitions.eps)

Se debe crear un directorio donde se especifican las expresiones regulares con las que han de casar los textos de las *features*, y dentro del bloque definiremos el código que comprobará que dicha frase es cierta.

Con *Cucumber* se consiguen unas pruebas en un idioma muy natural, aunque está pensado para usarse con Rails y programas interactivos. En concreto para este proyecto, se usa para probar la interfaz de comandos.

### Simplecov

*Simplecov* es una herramienta para obtener el cubrimiento que están realizando las pruebas. En un principio se intentó usar Coveralls, pero se descartó ya que era necesario tener el proyecto abierto al público. De forma alternativa, se ha conseguido desplegar la salida HTML de `simple-cov` en Gitlab mediante un Runner.

Prácticamente no tiene configuración inicial, y sólo se necesita requerirlo en el fichero `spec_helper`. La próxima vez que se ejecuten las pruebas se obtendrá un directorio llamado *"coverage"* donde estarán los ficheros HTML. Si se abre en el navegador, se podrá inspeccionar el porcentaje de cubrimiento que se ha realizado así como ver línea a línea qué no se ha cubierto.

## Control de Versiones y Alojamiento del Código Fuente

### Git

Se ha usado Git como control de versiones para administrar el código fuente del TFG, tanto de las dos gemas que se han creado como de la memoria. Se ha seguido el versionado semántico (*\Gls{semver}*) para el desarrollo de los tres proyectos, creando una etiqueta cada vez que se liberaba una nueva versión.

### Github

Github es el más famoso de los alojamientos de repositorios Git, con más de 19 millones de repositorios y casi seis millones de usuarios activos es la comunidad más grande de Git. Se había planteado usar Github en un principio, pero finalmente se ha usado Gitlab ya que ofrecía ventajas significativas.

### Gitlab

Gitlab es una plataforma similar a Github, pero además es un proyecto de código abierto.

#### Issues

Al igual que Github tenemos *"\glspl{issue}"*. Aquí es donde se pueden crear "tickets" para indicar problemas con el código fuente del proyecto, como errores, ideas, o recomendaciones. Contiene todo lo que ofrece Github, formato Markdown, milestones, tags, etc. Pero además tiene otras funciones que no tiene el primero, como peso para los "\glspl{issue}", el tiempo que ha consumido el issue e incluso un gráfico que indica la velocidad a la que vamos solucionando respecto al milestone.

#### Tablones de Issues

#### Wiki

En la wiki se puede añadir información respecto al proyecto en formato Markdown. Teniendo múltiples páginas en las que se puede añadir un manual o ejemplos de usos del proyecto.

#### Registro de Imágenes Docker

Cada repositorio de Gitlab permite tener un registro de imágenes \gls{docker}. Aquí se pueden subir las distintas imágenes que vayamos generando de nuestro proyecto para poder usarlas en producción. Además, estas imágenes se pueden generar desde los *\gls{runner}*, por lo que el proceso es automático.

#### Runners (CI y CD)

Los Runners de Gitlab son simplemente un flujo de imágenes de \gls{docker} que harán lo que le se haya indicado en el fichero de configuración `.gitlab-ci.yml`. Estos Runners se ejecutan cada vez subimos cambios al repositorio, de forma similar a como lo hace Travis.

En `dietary_dsl` se ha configurado el siguiente flujo de trabajo:

1. Se ejecutan dos tareas en paralelo:
    1. Se analiza el código con Rubocop
    2. Se ejecutan las pruebas de Rspec
2. Si las tareas anteriores son satisfactorias, se despliega el contenido de "coverage" en la web del repositorio y se da la confirmación de Git por válida.

Este flujo es sencillo para el desarrollo de una \gls{gema}, pero en un proyecto web donde son varios los pasos que se deben realizar resulta mucho más útil, hasta el punto en el que se pueden construir imágenes de Docker y desplegarlas en el servidor que sea necesario, como OpenShift Origin o Amazon. De esta forma se puede configurar una buena \gls{integracionContinua} y un \gls{continuousDelivery}.

Además, Gitlab proporciona dos insignias por defecto, una para el estado de la build del repositorio (*failed*, *running* y *success*) y una que indica el porcentaje de cubrimiento del código. En caso de necesitar alguna insignia más, podemos programar nosotros mismos alguna tarea que genere un fichero SVG y colocar su enlace donde necesitásemos.

#### Gitlab Comunity Edition y Enterprise Edition

Como se nombró al principio, Gitlab no sólo es un alojamiento para repositorios, sino que además es un proyecto de código abierto. Se puede descargar y desplegar en un servidor personal, tanto dedicado como en contenedores \gls{docker}, aunque son necesarios varios contenedores ya que, además del propio Gitlab, necesitaremos dos bases de datos, una de \gls{Redis} y otra de \gls{PostgreSQL}. En caso de desplegarlo sin Docker, Gitlab ofrece el paquete "Omnibus", que es un ejecutable que realiza toda la configuración necesaria para poder ejecutarlo correctamente.

Se ofrecen tres modalidades de Gitlab: Gitlab Comunity Edition, la Enterprise Edition Starter y la Enterprise Edition Premium. Estas dos últimos tienen algunas características más, e incluyen soporte 24/7 y cursos de iniciación.

#### Gitlab Pages

Se puede definir un \gls{runner} que se encargue de desplegar ficheros web en un subdominio de Gitlab, en concreto para este proyecto se han desplegado los análisis de cubrimiento de las gemas. Además de ficheros estáticos, también se puede desplegar contenido dinámico basado en Jekyll y sus plugins.

### RubyGems

RubyGems es el sitio web más común para distribuir gemas de Ruby. Se han publicado ambas gemas en este sitio, tanto `dietary_dsl` como `dietary_dsl_viewer`.

## Memoria

Esta memoria se ha escrito usando el lenguajde de marcado *"Markdown"* y luego mediante la herramienta *Pandoc* se convierte a *LaTeX* para finalmente obtener el fichero PDF. Se usan los \glspl{runner} de Gitlab para compilar la memoria en la nube y obtener un enlace directo al PDF resultante.

### LaTeX

LaTeX es un sistema de composición de textos orientado a la creación de documentos escritos que presenten una alta calidad tipográfica \cite{LatexWiki}. Con él se pueden escribir documentos en texto plano usando su lenguaje, y luego con alguna implementación de LaTeX compilarlo a un documento PDF. El problema (una de las razones por la que muchos no la usan) es su complejidad al querer conseguir algo, así como su sintaxis con llaves y barras invertidas que termina siendo algo difícil.

### Markdown

![Fragmento de esta memoria escrita en Markdown](../objects/markdown-memoria.eps)

Markdown es un lenguaje de marcado ligero y simple con el que se puede escribir documentos de forma sencilla con apenas casi sin sintaxis. De hecho en texto plano es legible. Son varias las herramientas que se pueden usar para convertir este lenguaje a uno que podamos visualizar, de entre ellas se ha escogido Pandoc.

### Pandoc

Pandoc es una herramienta escrita en Haskell para convertir de un lenguaje de marcado a otro. En este caso se usa para convertir los ficheros Markdown a LaTeX, de forma que el contenido de los capítulos está escrito en Markdown, y se usa una plantilla de LaTeX para el resto de la memoria.

Además, si es necesario Pandoc nos permite embeber código LaTeX dentro del fichero Markdown, por si se necesitara hacer algo que no se pueda hacer únicamente con Markdown.
