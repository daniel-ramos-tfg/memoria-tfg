# Presupuesto

Se propone el siguiente presupuesto para el desarrollo que ha sido realizado en este TFG.

| Tareas                               | Horas | Presupuesto |
|--------------------------------------|-------|-------------|
| Revisión bilbiográfica               | 30    | 5€/h        |
| Estudio de antecedentes              | 30    | 10€/h       |
| Diseño y desarrollo del ORM          | 60    | 20€/h       |
| Diseño y desarrollo del DSL          | 60    | 20€/h       |
| Diseño y desarrollo del visualizador | 60    | 20€/h       |

El coste total del proyecto es de 4050€, con una duración de 240 horas.
