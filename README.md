# Memoria de Lenguajes de Dominio Especı́fico para el Modelado de Menús Dietéticos

[![Build status](https://gitlab.com/DanielRamosAcosta/memoria-tfg/badges/master/build.svg)](https://gitlab.com/DanielRamosAcosta/memoria-tfg/pipelines)
[![Enlace de Descarga](https://img.shields.io/badge/PDF-download-brightgreen.svg)](https://gitlab.com/DanielRamosAcosta/memoria-tfg/builds/artifacts/master/raw/Memoria%20TFG.%20Lenguajes%20de%20Dominio%20Espec%C3%ADfico%20para%20el%20Modelado%20de%20Men%C3%BAs%20Diet%C3%A9ticos.%20Daniel%20Ramos.pdf?job=build)

Esta memoria se ha escrito usando el lenguaje de marcado Markdown, y luego se convierte a LaTeX mediante el uso de la herramienta [Pandoc](http://pandoc.org/). La memoria se compila mediante el CI de Gitlab, y se publica como artefacto. Se puede descagar en [este](https://gitlab.com/DanielRamosAcosta/memoria-tfg/builds/artifacts/master/raw/Memoria%20TFG.%20Lenguajes%20de%20Dominio%20Espec%C3%ADfico%20para%20el%20Modelado%20de%20Men%C3%BAs%20Diet%C3%A9ticos.%20Daniel%20Ramos.pdf?job=build) enlace.
